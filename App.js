import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import SecondLib from 'react-native-second-lib';

``;

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.parent}>
        <TouchableOpacity
          onPress={() => {
            console.log(
              JSON.stringify(
                SecondLib.sampleMethod('one','two', 2, x => {
                  console.log(x);
                }
                // , err => {
                //   console.log(err);
                // }
                ),
              ),
            );
          }}>
          <Text style={styles.tappableText}>Test</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  parent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
