package com.reactlibrary;

import android.app.AlertDialog;
import android.content.DialogInterface;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class SecondLibModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;

    public SecondLibModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "SecondLib";
    }

    @ReactMethod
    public void sampleMethod(String message,String title, int numberArgument, final Callback callback, final Callback errorCallback) {
        // TODO: Implement some actually useful functionality
//        Toast.makeText(reactContext, message, Toast.LENGTH_SHORT).show();
        callback.invoke("Received numberArgument: " + numberArgument + " stringArgument: " + message);
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentActivity());
            builder.setMessage(message)
                    .setTitle(title)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
//                        callback.invoke("Accepted");
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
//                        callback.invoke("Rejected");
                        }
                    });


// 3. Get the <code><a href="/reference/android/app/AlertDialog.html">AlertDialog</a></code> from <code><a href="/reference/android/app/AlertDialog.Builder.html#create()">create()</a></code>
            AlertDialog dialog = builder.create();
            dialog.show();
        } catch (Exception e){
            errorCallback.invoke(e.getMessage());
        }


    }
}
